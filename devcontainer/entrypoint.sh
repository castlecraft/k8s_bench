#!/bin/bash

if [ ! -f "${KUBECONFIG}" ]; then
	curl -sSL http://k3s:8081 >"${KUBECONFIG}"
	chmod go-r "${KUBECONFIG}"
fi

exec "$@"
