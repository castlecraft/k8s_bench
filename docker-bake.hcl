variable "VERSION" {
  default = "latest"
}

variable "REGISTRY_NAME" {
  default = "registry.gitlab.com/castlecraft"
}

variable "IMAGE_NAME" {
  default = "k8s_bench"
}

target "default" {
    dockerfile = "image/Containerfile"
    tags = ["${REGISTRY_NAME}/${IMAGE_NAME}:${VERSION}"]
}
