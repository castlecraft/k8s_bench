#!./env/bin/python
import argparse
import json
import os
import subprocess
import sys

import git
import ruamel.yaml
import semantic_version

from k8s_bench.main import app

NO_CHANGES_DRY_RUN = "No changes will be made as --dry-run was specified."

cli_print = print  # noqa: T002,T202


def run():
    parser = get_parse_args()
    args = parser.parse_args()

    if not args.chart and not args.app:
        parser.print_help()
        return

    if not args.dry_run:
        validate_repo(args=args)
        validate_bump(args=args)

    if args.dry_run:
        cli_print(NO_CHANGES_DRY_RUN)

    new_app_version = None
    new_chart_version = None

    if args.app:
        # bump up __version__
        new_app_version = save_new_app_version(args=args)

        # Generate version change in docs/openapi.json
        generate_openapi_json_file(args)

    if args.chart:
        # bump up Chart.yaml
        new_chart_version = update_chart(args, new_app_version)

        # Generate README from values.yaml comments
        generate_frigate_docs(args)

    # tag git
    if not args.dry_run:
        git_commit_tag_push(
            args=args,
            new_app_version=new_app_version,
            new_chart_version=new_chart_version,
        )


def get_parse_args():
    parser = argparse.ArgumentParser(
        description="helm chart release script for k8s bench", add_help=True
    )
    parser.add_argument(
        "-c",
        "--chart",
        action="store",
        type=str,
        choices=["major", "minor", "patch"],
        help="Bump Type for Helm Chart",
    )
    parser.add_argument(
        "-a",
        "--app",
        action="store",
        type=str,
        choices=["major", "minor", "patch"],
        help="Bump Type for k8s bench",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="DO NOT make changes",
    )
    group = parser.add_argument_group("options")
    group.add_argument(
        "--remote",
        action="store",
        type=str,
        default="origin",
        help="Git remote to tag and release helm chart",
    )

    return parser


def get_yaml_dict(file_name):
    yaml_string = get_yaml_file(file_name)
    yaml_dict = ruamel.yaml.round_trip_load(yaml_string, preserve_quotes=True)
    return yaml_dict


def get_yaml_file(file_name):
    data_string = None
    try:
        with open(file_name) as f:
            data_string = f.read()
    except Exception as exc:
        cli_print(exc)
        sys.exit(1)
    return data_string


def save_yaml_file(data, file_name):
    with open(file_name, "w") as f:
        ruamel.yaml.round_trip_dump(data, f)


def update_chart(
    args: argparse.Namespace,
    new_app_version: str = None,
):
    # Update Chart.yaml
    chart_file = os.path.join("helm-chart", "Chart.yaml")
    chart_dict = get_yaml_dict(chart_file)

    if not args.chart:
        return

    current_chart_version = semantic_version.Version(chart_dict["version"])
    bumped_chart_version = get_bumped_version(
        args.chart,
        current_chart_version,
    )
    current_app_version = semantic_version.Version(chart_dict["appVersion"])
    chart_dict["version"] = str(bumped_chart_version)
    cli_print(
        f"""Bump version from {
            current_chart_version
        } to {
            str(bumped_chart_version)
        } in {chart_file}"""
    )
    if new_app_version:
        chart_dict["appVersion"] = str(new_app_version)
        cli_print(
            f"""Bump appVersion from {
                current_app_version
            } to {
                new_app_version
            } in {chart_file}"""
        )

    if args.dry_run:
        return chart_dict["version"]

    save_yaml_file(chart_dict, chart_file)

    return chart_dict["version"]


def git_commit_tag_push(
    args: argparse.Namespace,
    new_app_version: str = None,
    new_chart_version: str = None,
):
    if not new_app_version and not new_chart_version:
        return

    repo = git.Repo(os.getcwd())

    commit_message = "chore: publish release\n"
    if new_app_version:
        commit_message += f"\napp version: {new_app_version}"
    if new_chart_version:
        commit_message += f"\nhelm chart version: {new_chart_version}"

    cli_print(f"git tag: {new_app_version}")
    cli_print("")
    cli_print("Commit, tag and push to git repository with commit message:")
    cli_print("")
    cli_print(commit_message)

    if args.dry_run:
        return

    repo.git.add(all=True)
    repo.git.commit("-m", commit_message)

    # tag if --app bump
    if new_app_version:
        repo.create_tag(new_app_version, message=f"Released {new_app_version}")

    git_push_all(repo, args.remote)


def git_push_all(repo: git.Repo, remote: str):
    git_ssh_command = os.environ.get("GIT_SSH_COMMAND")
    if git_ssh_command:
        repo.git.update_environment(GIT_SSH_COMMAND=git_ssh_command)
    repo.git.push(remote, "--follow-tags")


def get_bumped_version(bump_type, current_version: semantic_version.Version):
    bumped_version = current_version
    if bump_type == "major":
        bumped_version = bumped_version.next_major()
    if bump_type == "minor":
        bumped_version = bumped_version.next_minor()
    if bump_type == "patch":
        bumped_version = bumped_version.next_patch()

    return str(bumped_version)


def get_current_app_version(init_file=None):
    if not init_file:
        init_file = get_init_file()

    with open(init_file, encoding="utf-8") as f:
        for line in f.readlines():
            current_app_version = semantic_version.Version(
                line.replace(
                    "__version__ = ",
                    "",
                )
                .replace("\n", "")
                .replace('"', "")
            )

    return current_app_version


def get_current_chart_version():
    chart_file = os.path.join("helm-chart", "Chart.yaml")
    chart_dict = get_yaml_dict(chart_file)
    return semantic_version.Version(chart_dict["version"])


def save_new_app_version(args: argparse.Namespace):
    init_file = get_init_file()
    current_app_version = get_current_app_version(init_file=init_file)
    new_app_version = get_bumped_version(args.app, current_app_version)
    cli_print(
        f"""Bump version from {
            current_app_version
        } to {
            new_app_version
        } in {init_file}"""
    )

    if args.dry_run:
        return new_app_version

    with open(init_file, "w") as version_file:
        version_file.write(f'__version__ = "{new_app_version}"\n')

    return new_app_version


def validate_repo(args):
    repo = git.Repo(os.getcwd())
    if str(repo.active_branch) != "main":
        cli_print("Make sure you are on main branch")
        sys.exit(1)

    if repo.is_dirty():
        cli_print("Make sure you have committed changes")
        sys.exit(1)


def validate_bump(args):
    is_bump_invalid = False
    invalid_bump_error = "Invalid Bump\n"

    if args.app:
        current_app_version = get_current_app_version()
        bump_app_version = get_bumped_version(
            args.app,
            current_app_version,
        )
        if semantic_version.Version(bump_app_version) <= current_app_version:
            is_bump_invalid = True
            invalid_bump_error += f"Cannot bump app from {current_app_version} to {bump_app_version}\n"  # noqa: E501

    if args.chart:
        current_chart_version = get_current_chart_version()
        bump_chart_version = get_bumped_version(
            args.chart,
            current_chart_version,
        )
        if (
            semantic_version.Version(bump_chart_version)
            <= current_chart_version  # noqa: E501
        ):  # noqa: E501
            is_bump_invalid = True
            invalid_bump_error += f"Cannot bump chart from {current_chart_version} to {bump_chart_version}\n"  # noqa: E501

    if is_bump_invalid:
        cli_print(invalid_bump_error)
        sys.exit(1)


def get_init_file():
    return os.path.join(
        "k8s_bench",
        "__init__.py",
    )


def generate_openapi_json_file(args):
    openapi = {}
    openapi_file = os.path.join("docs", "openapi.json")

    with open(openapi_file, "r") as in_file:
        openapi = json.load(in_file)
    current_app_version = openapi.get("info", {}).get("version")
    bumped_app_version = get_bumped_version(
        args.app, semantic_version.Version(current_app_version)
    )
    cli_print(
        f"""Bump info.version from {
            current_app_version
        } to {
            bumped_app_version
        } in {openapi_file}"""
    )
    if args.dry_run:
        return

    openapi = app.openapi()
    openapi["info"]["version"] = bumped_app_version
    with open(openapi_file, "w") as outfile:
        cli_print(json.dumps(openapi), file=outfile)


def generate_frigate_docs(args: argparse.Namespace):
    command = (
        "./env/bin/frigate",
        "gen",
        "--output-format=markdown",
        "--no-credits",
        "helm-chart",
    )
    readme = subprocess.check_output(command, encoding="UTF-8").rstrip()
    if not args.dry_run:
        with open("helm-chart/README.md", "w") as outfile:
            cli_print(readme, file=outfile)


if __name__ == "__main__":
    run()
