#!/bin/bash

set -e

IMAGE_NAME=registry.gitlab.com/castlecraft/k8s_bench/custom-image
if [ -z "${CI_REGISTRY}" ]; then
	IMAGE_NAME=registry:5000/custom-image
fi
export IMAGE_NAME

# Create frappe-bench-exec pod
echo -e "\033[1mCreate frappe-bench-exec Pod\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/create-pod \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
        \"name\": \"frappe-bench-exec\",
        \"namespace\": \"erpnext\",
        \"image\": \"${IMAGE_NAME}:latest\",
        \"sites_pvc\": \"frappe-bench-erpnext\",
        \"annotations\": {},
        \"labels\": {},
        \"command\": [],
        \"args\": [
          \"tail\",
          \"-f\",
          \"/dev/null\"
        ],
        \"image_pull_policy\": \"IfNotPresent\",
        \"image_pull_secrets\": [{\"name\": \"reg-cred\"}],
        \"ctr_name\": \"console\",
        \"resources\": {},
        \"logs_pvc\": null
      }" | jq .
echo -e ""

# Wait for pod to be ready
kubectl -n erpnext wait --for=condition=ready pod frappe-bench-exec

# Get Pod Status
echo -e "\033[1mGet frappe-bench-exec Pod Status\033[0m"
curl -fsS "http://0.0.0.0:8000/core/get-pod-status?name=frappe-bench-exec&namespace=erpnext" \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' | jq .
echo -e ""

# Check terminal exec
./env/bin/python tests/api/terminal_ws.py

# Delete pod
echo -e "\033[1mDelete frappe-bench-exec Pod\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/core/delete-pod \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "frappe-bench-exec",
        "namespace": "erpnext"
      }' | jq .
echo -e ""
