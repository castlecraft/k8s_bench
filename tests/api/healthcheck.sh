#!/bin/bash

set -e

# Ping healthcheck
echo -e "\033[1mPing healthcheck\033[0m"
curl -fsS http://0.0.0.0:8000/healthz | jq .
echo -e ""
