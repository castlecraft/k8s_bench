#!/bin/bash

set -e

IMAGE_NAME=registry.gitlab.com/castlecraft/k8s_bench/custom-image
if [ -z "${CI_REGISTRY}" ]; then
	IMAGE_NAME=registry:5000/custom-image
fi
export IMAGE_NAME

# Initiate bench build
echo -e "\033[1mInitiate bench build\033[0m"
REGISTRY_IP=$(ping -c 2 registry | awk -F '[()]' '/PING/ { print $2}')
export REGISTRY_IP
curl -fsS -X POST http://0.0.0.0:8000/jobs/build-bench \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
    \"job_name\": \"build-custom-bench\",
    \"namespace\": \"builder\",
    \"compressed_cache\": false,
    \"push_secret_name\": \"reg-cred\",
    \"destination_image_name\": \"${IMAGE_NAME}:latest\",
    \"container_file_path\": \"images/custom/Containerfile\",
    \"frappe_path\": \"https://github.com/frappe/frappe\",
    \"frappe_branch\": \"version-15\",
    \"python_version\": \"3.12.8\",
    \"node_version\": \"22.12.0\",
    \"apps_json\": [
      {\"url\":\"https://github.com/castlecraft/cfe\",\"branch\":\"main\"}
    ],
    \"git_repo_context\": \"github.com/frappe/frappe_docker\",
    \"insecure_registry\": true,
    \"use_new_run\": true,
    \"snapshot_mode\": \"redo\",
    \"host_aliases\": [
      {\"hostnames\":[\"registry\"],\"ip\":\"$REGISTRY_IP\"}
    ]
  }" | jq .
echo -e ""
echo -e "\033[1mWaiting for build-custom-bench to complete\033[0m"
JOBUUID=$(kubectl -n builder get job build-custom-bench -o "jsonpath={.metadata.labels.controller-uid}")
export JOBUUID
PODNAME=$(kubectl -n builder get pod -l controller-uid="${JOBUUID}" -o name)
export PODNAME
echo "Job uuid ${JOBUUID}"
echo "Pod name ${PODNAME}"
kubectl -n builder wait --timeout=900s --for=condition=ready "${PODNAME}"

# Test get job-status api
echo -e "\033[1mCheck job-status\033[0m"
curl -fsS "http://0.0.0.0:8000/jobs/get-status?job_name=build-custom-bench&namespace=builder" \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' | jq .
echo -e ""

echo "Logs..."
kubectl -n builder logs job/build-custom-bench -f
kubectl -n builder wait --timeout=900s --for=condition=complete job/build-custom-bench
# Delete Jobs from builder namespace
echo -e "\033[1mDelete Jobs from builder namespace\033[0m"
kubectl delete jobs -n builder --all
