#!/bin/bash

set -e

echo -e "\033[1mCreate Ingress\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/ingress/create \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
    "name": "test-ingress",
    "namespace": "erpnext",
    "host": "frappe.localhost",
    "service_name": "frappe-bench-erpnext",
    "service_port": "8080",
    "annotations": { "cert-manager.io/cluster-issuer": "letsencrypt-prod" }
  }' | jq .
echo -e ""

echo -e "\033[1mConfirm Ingress\033[0m"
./tests/operator/ingress_watch.py --name test-ingress --namespace erpnext create
curl -fsS "http://0.0.0.0:8000/ingress/get-status?name=test-ingress&namespace=erpnext" \
	-u 'admin:changeit' | jq .
echo -e ""
echo -e "\033[1mPatch Ingress\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/ingress/patch \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
    "name": "test-ingress",
    "namespace": "erpnext",
    "service_name": "frappe-bench-erpnext-gunicorn",
    "service_port": "8000"
  }' | jq .
echo -e ""
./tests/operator/ingress_watch.py --name test-ingress --namespace erpnext update
INGRESS_SVC=$(kubectl get ingress -n erpnext test-ingress -o jsonpath="{.spec.rules[0].http.paths[0].backend.service.name}")
export INGRESS_SVC
if [ "${INGRESS_SVC}" != "frappe-bench-erpnext-gunicorn" ]; then
	echo "incorrect host"
	exit 1
fi
echo -e "\033[1mDelete Ingress\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/ingress/delete \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
    "name": "test-ingress",
    "namespace": "erpnext"
  }' | jq .
./tests/operator/ingress_watch.py --name test-ingress --namespace erpnext delete
