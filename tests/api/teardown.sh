#!/bin/bash

# Uninstall flux
echo -e "\033[1mUninstall Flux\033[0m"
flux uninstall -s

# Uninstall NFS Helm Release
echo -e "\033[1mUninstall NFS Helm Release\033[0m"
helm uninstall -n nfs in-cluster --wait
kubectl delete ns nfs
