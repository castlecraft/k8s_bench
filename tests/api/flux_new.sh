#!/bin/bash

set -e

IMAGE_NAME=registry.gitlab.com/castlecraft/k8s_bench/custom-image
if [ -z "${CI_REGISTRY}" ]; then
	IMAGE_NAME=registry:5000/custom-image
fi
export IMAGE_NAME

# Create helm release source
echo -e "\033[1mCreate helm release source\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/flux/add-helmsource \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d '{
        "name": "frappe",
        "namespace": "builder",
        "kind": "HelmRepository",
        "interval": "5m0s",
        "url": "https://helm.erpnext.com"
      }' | jq .

# Add Flux Bench
echo -e "\033[1mAdd Flux Bench\033[0m"
curl -fsS -X POST http://0.0.0.0:8000/flux/add-helmrelease \
	-H 'Content-Type: application/json' \
	-u 'admin:changeit' \
	-d "{
  \"name\": \"frappe-bench\",
  \"namespace\": \"erpnext\",
  \"release_interval\": \"60s\",
  \"chart_interval\": \"60s\",
  \"chart\": \"erpnext\",
  \"source_ref\": {
        \"kind\": \"HelmRepository\",
        \"name\": \"frappe\",
        \"namespace\": \"builder\"
  },
  \"values\": {
    \"image\": { \"repository\": \"${IMAGE_NAME}\", \"tag\": \"latest\" },
    \"persistence\": { \"worker\": { \"storageClass\": \"nfs\" } },
    \"jobs\": { \"configure\": { \"fixVolume\": false } },
    \"imagePullSecrets\": [{ \"name\": \"reg-cred\" }]
  },
  \"remediate_last_failure\": true,
  \"timeout\": \"15m\"
}" | jq .

echo -e ""
echo -e "\033[1mWaiting for helm release to be ready\033[0m"
kubectl -n erpnext wait --timeout=1800s --for=condition=ready helmreleases frappe-bench
