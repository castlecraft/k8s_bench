#!/bin/bash

set -e

echo -e "\033[1mGet Flux Bench Status\033[0m"
curl -fsS "http://0.0.0.0:8000/flux/get-helmrelease-status?name=frappe-bench&namespace=erpnext" \
	-u 'admin:changeit' | jq .
echo -e ""
