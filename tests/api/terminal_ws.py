#!./env/bin/python
import logging

import requests
import websocket

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("terminal-websocket")


def main():
    token = None
    res = requests.get(
        url="http://0.0.0.0:8000/terminal/get-short-token",
        auth=("admin", "changeit"),
    )
    token = res.json().get("token")
    ws = websocket.WebSocket()
    ws.connect(
        f"ws://0.0.0.0:8000/terminal/ws/erpnext/frappe-bench-exec?command=bash&token={token}"  # noqa: E501
    )
    command = "echo hello"
    log.info(f"command: {command}")
    ws.send(command)
    cmd_out = str(ws.recv()).strip()
    log.info(f"output: {cmd_out}")
    assert "hello" in cmd_out
    ws.close()


if __name__ == "__main__":
    main()
