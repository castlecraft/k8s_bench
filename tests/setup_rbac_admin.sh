#!/bin/bash

# Reset KUBECONIFG
mkdir -p "${HOME}"/.kube
KUBECONFIG="${HOME}"/.kube/config
export KUBECONFIG
curl -s k3s:8081 >"${KUBECONFIG}"
