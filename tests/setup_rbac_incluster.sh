#!/bin/bash

# Create RBAC
echo -e "\033[1mSetup RBAC\033[0m"
helm template \
	--create-namespace \
	--namespace default \
	--set api.enabled=true \
	--set api.apiKey=admin \
	--set api.apiSecret=changeit \
	--set api.createFluxRBAC=true \
	-s templates/rbac.yaml \
	-s templates/serviceaccount.yaml \
	manage-sites helm-chart | kubectl apply -f -

# the name of the secret containing the service account token goes here
SVCAC=$(kubectl get secret | grep manage-sites-k8s-bench-token | awk '{print $1}')
export SVCAC

CA=$(kubectl get secret/"${SVCAC}" -o jsonpath='{.data.ca\.crt}')
export CA

TOKEN=$(kubectl get secret/"${SVCAC}" -o jsonpath='{.data.token}' | base64 -d)
export TOKEN

# Reset KUBECONIFG
KUBECONFIG="${HOME}"/.kube/config
export KUBECONFIG

echo "
apiVersion: v1
kind: Config
clusters:
- name: k3s-bench
  cluster:
    certificate-authority-data: ${CA}
    server: https://k3s:6443
contexts:
- name: k3s-bench-context
  context:
    cluster: k3s-bench
    namespace: default
    user: manage-sites-k8s-bench
current-context: k3s-bench-context
users:
- name: manage-sites-k8s-bench
  user:
    token: ${TOKEN}
" >"${KUBECONFIG}"
