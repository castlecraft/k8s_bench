import typing

from kubernetes import client

from k8s_bench.client.utils import load_config, to_dict


def get_cronjob_status(name: str, namespace: str):
    load_config()
    batch_v1_api = client.BatchV1Api()
    api_response = batch_v1_api.read_namespaced_cron_job_status(
        name=name,
        namespace=namespace,
    )
    return to_dict(api_response)


def add_cronjob(
    name: str,
    namespace: str,
    sites_pvc: str,
    cronstring: str,
    args: typing.Union[str, typing.List[str]],
    image: str = None,
    backoff_limit: int = 0,
    command: typing.Union[str, typing.List[str]] = None,
    logs_pvc: str = None,
    image_pull_secrets: typing.List[dict[str, str]] = None,
    annotations: dict = None,
    node_selector: client.V1NodeSelector = None,
    resources: client.V1ResourceRequirements = None,
    env_vars: typing.List[client.V1EnvVar] = None,
):
    load_config()

    if isinstance(args, str):
        args = args.split(" ")

    batch_v1_api = client.BatchV1Api()
    body = client.V1CronJob(api_version="batch/v1", kind="CronJob")

    body.metadata = client.V1ObjectMeta(
        namespace=namespace,
        name=name,
        annotations=annotations,
    )

    body.status = client.V1CronJobStatus()

    volumes = [
        client.V1Volume(
            name="sites-dir",
            persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(
                claim_name=sites_pvc,
                read_only=False,
            ),
        ),
    ]

    volume_mounts = [
        client.V1VolumeMount(
            name="sites-dir",
            mount_path="/home/frappe/frappe-bench/sites",
        ),
    ]

    if logs_pvc:
        volumes.append(
            client.V1Volume(
                name="logs",
                persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(  # noqa: E501
                    claim_name=logs_pvc,
                    read_only=False,
                ),
            ),
        )
        volume_mounts.append(
            client.V1VolumeMount(
                name="logs",
                mount_path="/home/frappe/frappe-bench/logs",
            ),
        )

    image = image or "frappe/erpnext:latest"

    body.spec = client.V1CronJobSpec(
        schedule=cronstring,
        job_template=client.V1JobTemplateSpec(
            spec=client.V1JobSpec(
                backoff_limit=backoff_limit,
                template=client.V1PodTemplateSpec(
                    spec=client.V1PodSpec(
                        security_context=client.V1PodSecurityContext(
                            supplemental_groups=[1000]
                        ),
                        node_selector=node_selector,
                        containers=[
                            client.V1Container(
                                name="bench",
                                image=image,
                                env=env_vars,
                                command=command,
                                args=args,
                                volume_mounts=volume_mounts,
                                resources=resources,
                            ),
                        ],
                        restart_policy="Never",
                        volumes=volumes,
                        image_pull_secrets=image_pull_secrets,
                    )
                ),
            ),
        ),
    )

    api_response = batch_v1_api.create_namespaced_cron_job(
        namespace=namespace,
        body=body,
        pretty=True,
    )

    return to_dict(api_response)


def delete_cronjob(name: str, namespace: str):
    load_config()
    batch_v1_api = client.BatchV1Api()
    api_response = batch_v1_api.delete_namespaced_cron_job(
        name=name,
        namespace=namespace,
    )
    return to_dict(api_response)
