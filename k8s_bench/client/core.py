import typing

from kubernetes import client
from kubernetes.stream import stream

from k8s_bench.client.utils import load_config, to_dict


def add_namespace(name):
    load_config()
    body = client.V1Namespace(metadata=client.V1ObjectMeta(name=name))
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.create_namespace(body=body)
    return to_dict(api_response)


def delete_namespace(name):
    load_config()
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.delete_namespace(name=name)
    return to_dict(api_response)


def add_secret(
    name: str,
    namespace: str,
    string_data: dict,
    secret_type: str | None = None,
):
    load_config()
    body = client.V1Secret(
        metadata=client.V1ObjectMeta(name=name, namespace=namespace),
        string_data=string_data,
        type=secret_type,
    )
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.create_namespaced_secret(
        namespace=namespace,
        body=body,
    )
    return to_dict(api_response)


def delete_secret(name: str, namespace: str):
    load_config()
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.delete_namespaced_secret(
        name=name,
        namespace=namespace,
    )
    return to_dict(api_response)


def update_secret(
    name: str,
    namespace: str,
    string_data: dict,
    secret_type: str | None = None,
):
    load_config()
    body = client.V1Secret(
        metadata=client.V1ObjectMeta(name=name, namespace=namespace),
        string_data=string_data,
        type=secret_type,
    )
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.patch_namespaced_secret(
        name=name,
        namespace=namespace,
        body=body,
    )
    return to_dict(api_response)


def get_terminal(
    name: str, namespace: str, command: list[str], container: str | None = None
):
    load_config()
    core_v1_api = client.CoreV1Api()
    return stream(
        core_v1_api.connect_get_namespaced_pod_exec,
        name=name,
        namespace=namespace,
        container=container,
        command=command,
        stderr=True,
        stdin=True,
        stdout=True,
        tty=False,
        _preload_content=False,
    )


def create_pod(
    name: str,
    namespace: str,
    image: str,
    sites_pvc: str,
    annotations: dict = {},
    labels: dict = {},
    command: list[str] = [],
    args: list[str] = [],
    image_pull_policy: str = "Always",
    ctr_name: str = "console",
    resources: client.V1ResourceRequirements | None = None,
    logs_pvc: str | None = None,
    image_pull_secrets: typing.List[dict[str, str]] | None = None,
):
    load_config()
    core_v1_api = client.CoreV1Api()
    body = client.V1Pod()
    body.metadata = client.V1ObjectMeta(
        annotations=annotations,
        name=name,
        labels=labels,
    )

    volumes = [
        client.V1Volume(
            name="sites-dir",
            persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(
                claim_name=sites_pvc,
                read_only=False,
            ),
        ),
    ]

    volume_mounts = [
        client.V1VolumeMount(
            name="sites-dir",
            mount_path="/home/frappe/frappe-bench/sites",
        ),
    ]

    if logs_pvc:
        volumes.append(
            client.V1Volume(
                name="logs",
                persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(  # noqa: E501
                    claim_name=logs_pvc,
                    read_only=False,
                ),
            ),
        )
        volume_mounts.append(
            client.V1VolumeMount(
                name="logs",
                mount_path="/home/frappe/frappe-bench/logs",
            ),
        )

    body.spec = client.V1PodSpec(
        containers=[
            client.V1Container(
                image=image,
                image_pull_policy=image_pull_policy,
                name=ctr_name,
                command=command,
                args=args,
                resources=resources,
                security_context=client.V1SecurityContext(),
                volume_mounts=volume_mounts,
            ),
        ],
        volumes=volumes,
        image_pull_secrets=image_pull_secrets,
    )

    api_response = core_v1_api.create_namespaced_pod(
        namespace=namespace,
        body=body,
    )

    return to_dict(api_response)


def delete_pod(name: str, namespace: str):
    load_config()
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.delete_namespaced_pod(
        name=name,
        namespace=namespace,
    )

    return to_dict(api_response)


def get_pod_status(name: str, namespace: str):
    load_config()
    core_v1_api = client.CoreV1Api()
    api_response = core_v1_api.read_namespaced_pod_status(
        name=name,
        namespace=namespace,
    )

    return to_dict(api_response)
