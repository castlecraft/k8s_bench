from k8s_bench.client.utils import load_config, to_dict
from kubernetes import client


def create_ingress(
    ingress_name: str,
    namespace: str,
    host: str,
    service_name: str,
    service_port: int,
    cert_secret_name: str,
    is_wildcard: bool,
    annotations: dict[str, str] = None,
):
    load_config()

    # annotations = annotations or { "kubernetes.io/ingress.class": "nginx" }
    namespace = namespace or "default"
    net_v1_api = client.NetworkingV1Api()
    hosts = [host]

    if is_wildcard:
        hosts.append(f"*.{host}")

    body = client.V1Ingress(
        api_version="networking.k8s.io/v1",
        kind="Ingress",
        metadata=client.V1ObjectMeta(
            name=ingress_name,
            annotations=annotations,
            namespace=namespace,
        ),
        spec=client.V1IngressSpec(
            rules=[
                client.V1IngressRule(
                    host=host,
                    http=client.V1HTTPIngressRuleValue(
                        paths=[
                            client.V1HTTPIngressPath(
                                path="/",
                                path_type="ImplementationSpecific",
                                backend=client.V1IngressBackend(
                                    service=client.V1IngressServiceBackend(
                                        port=client.V1ServiceBackendPort(
                                            number=service_port,
                                        ),
                                        name=service_name,
                                    )
                                ),
                            )
                        ]
                    ),
                )
            ],
            tls=[
                client.V1IngressTLS(
                    hosts=hosts,
                    secret_name=cert_secret_name,
                )
            ],
        ),
    )
    api_response = net_v1_api.create_namespaced_ingress(
        namespace=namespace,
        body=body,
    )
    return to_dict(api_response)


def get_ingress(
    ingress_name,
    namespace=None,
):
    load_config()
    namespace = namespace or "default"
    net_v1_api = client.NetworkingV1Api()
    api_response = net_v1_api.read_namespaced_ingress(
        name=ingress_name,
        namespace=namespace,
    )
    return to_dict(api_response)


def patch_ingress(
    ingress_name: str,
    namespace: str,
    service_name: str,
    service_port: int,
):
    load_config()

    # annotations = annotations or { "kubernetes.io/ingress.class": "nginx" }
    namespace = namespace or "default"
    net_v1_api = client.NetworkingV1Api()
    body = client.V1Ingress(
        api_version="networking.k8s.io/v1",
        kind="Ingress",
        metadata=client.V1ObjectMeta(
            name=ingress_name,
            namespace=namespace,
        ),
        spec=client.V1IngressSpec(
            rules=[
                client.V1IngressRule(
                    http=client.V1HTTPIngressRuleValue(
                        paths=[
                            client.V1HTTPIngressPath(
                                path="/",
                                path_type="ImplementationSpecific",
                                backend=client.V1IngressBackend(
                                    service=client.V1IngressServiceBackend(
                                        port=client.V1ServiceBackendPort(
                                            number=service_port,
                                        ),
                                        name=service_name,
                                    )
                                ),
                            )
                        ]
                    ),
                )
            ],
        ),
    )
    api_response = net_v1_api.patch_namespaced_ingress(
        name=ingress_name,
        namespace=namespace,
        body=body,
    )
    return to_dict(api_response)


def delete_ingress(
    ingress_name: str,
    namespace: str,
):
    load_config()

    # annotations = annotations or { "kubernetes.io/ingress.class": "nginx" }
    namespace = namespace or "default"
    net_v1_api = client.NetworkingV1Api()
    api_response = net_v1_api.delete_namespaced_ingress(
        namespace=namespace, name=ingress_name
    )
    return to_dict(api_response)


def get_ingress_status(
    ingress_name,
    namespace=None,
):
    load_config()
    namespace = namespace or "default"
    net_v1_api = client.NetworkingV1Api()
    api_response = net_v1_api.read_namespaced_ingress_status(
        name=ingress_name,
        namespace=namespace,
    )
    return to_dict(api_response)
