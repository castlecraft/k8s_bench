from datetime import datetime
import os
from kubernetes import config


def to_dict(obj):
    if hasattr(obj, "attribute_map"):
        result = {}
        for k, v in getattr(obj, "attribute_map").items():
            val = getattr(obj, k)
            if val is not None:
                result[v] = to_dict(val)
        return result
    elif isinstance(obj, list):
        return [to_dict(x) for x in obj]
    elif isinstance(obj, datetime):
        return str(obj)
    else:
        return obj


def load_config():
    if os.environ.get("KUBECONFIG"):
        config.load_kube_config()
    else:
        config.load_incluster_config()
