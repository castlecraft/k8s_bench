import base64
import json
import typing

from kubernetes import client

from k8s_bench.client.utils import load_config, to_dict


def bench_command_job(
    job_name: str,
    sites_pvc: str,
    args: typing.Union[str, typing.List[str]],
    command: typing.Union[str, typing.List[str]] = None,
    logs_pvc: str = None,
    namespace: str = None,
    image_pull_secrets: typing.List[dict[str, str]] = None,
    image: str = None,
    annotations: dict = None,
    node_selector: client.V1NodeSelector = None,
    resources: client.V1ResourceRequirements = None,
    additional_volumes: typing.List[dict] = None,
):
    """Create job to execute bench command

    Args:
        job_name: name of the job to be created, must be unique.
        sites_pvc: name of the PVC for sites
        args: string or list of container args for job container
        command: entrypoint for job container. Defaults to None.
        logs_pvc: name of the PVC for logs. Defaults to None.
        namespace: namespace to execute job in. Defaults to None.
        image_pull_secrets: list of image pull secrets. Defaults to None.
        worker_image: name of worker image to override. Defaults to None.
        nginx_image: name of nginx image to override. Defaults to None.

    Returns:
        api_response: job response converted to dict
    """
    load_config()

    if isinstance(args, str):
        args = args.split(" ")

    batch_v1_api = client.BatchV1Api()
    namespace = namespace or "default"
    image = image or "frappe/erpnext:v14.14.0"
    body = client.V1Job(api_version="batch/v1", kind="Job")

    body.metadata = client.V1ObjectMeta(
        namespace=namespace,
        name=job_name,
        annotations=annotations,
    )

    body.status = client.V1JobStatus()

    volumes = [
        client.V1Volume(
            name="sites-dir",
            persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(
                claim_name=sites_pvc,
                read_only=False,
            ),
        ),
    ]

    bench_volume_mounts = [
        client.V1VolumeMount(
            name="sites-dir",
            mount_path="/home/frappe/frappe-bench/sites",
        ),
    ]

    if logs_pvc:
        volumes.append(
            client.V1Volume(
                name="logs",
                persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(  # noqa: E501
                    claim_name=logs_pvc,
                    read_only=False,
                ),
            ),
        )
        bench_volume_mounts.append(
            client.V1VolumeMount(
                name="logs",
                mount_path="/home/frappe/frappe-bench/logs",
            ),
        )

    if additional_volumes:
        for vol in additional_volumes:
            volumes.append(
                client.V1Volume(
                    name=vol.get("volume_name"),
                    persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(  # noqa: E501
                        claim_name=vol.get("pvc_name"),
                        read_only=vol.get("read_only", False),
                    ),
                ),
            )
            bench_volume_mounts.append(
                client.V1VolumeMount(
                    name=vol.get("volume_name"),
                    mount_path=vol.get("mount_path"),
                ),
            )

    body.spec = client.V1JobSpec(
        backoff_limit=0,
        template=client.V1PodTemplateSpec(
            spec=client.V1PodSpec(
                security_context=client.V1PodSecurityContext(
                    supplemental_groups=[1000]
                ),
                node_selector=node_selector,
                containers=[
                    client.V1Container(
                        name="bench",
                        image=image,
                        command=command,
                        args=args,
                        volume_mounts=bench_volume_mounts,
                        resources=resources,
                    ),
                ],
                restart_policy="Never",
                volumes=volumes,
                image_pull_secrets=image_pull_secrets,
            )
        ),
    )

    api_response = batch_v1_api.create_namespaced_job(
        namespace=namespace,
        body=body,
        pretty=True,
    )

    return to_dict(api_response)


def get_job_status(job_name: str, namespace: str = None):
    load_config()
    batch_v1_api = client.BatchV1Api()
    api_response = batch_v1_api.read_namespaced_job_status(
        name=job_name,
        namespace=namespace or "erpnext",
    )
    return to_dict(api_response)


def annotate_job(job_name: str, namespace: str, annotations: dict):
    load_config()
    body = client.V1Job(api_version="batch/v1", kind="Job")
    body.metadata = client.V1ObjectMeta(
        namespace=namespace,
        name=job_name,
        annotations=annotations,
    )
    batch_v1_api = client.BatchV1Api()
    api_response = batch_v1_api.patch_namespaced_job(
        namespace=namespace,
        name=job_name,
        body=body,
    )
    return to_dict(api_response)


def build_bench_image(
    job_name: str,
    namespace: str,
    push_secret_name: str,
    destination_image_name: str,
    container_file_path: str = None,
    frappe_path: str = None,
    frappe_branch: str = None,
    python_version: str = None,
    node_version: str = None,
    apps_json: typing.List[dict] = None,
    git_repo_context: str = None,
    node_selector: client.V1NodeSelector = None,
    resources: client.V1ResourceRequirements = None,
    insecure_registry: bool = None,
    host_aliases: typing.List[client.V1HostAlias] = None,
    snapshot_mode: str = None,
    use_new_run: bool = None,
    cache: bool = None,
    kaniko_image: str = None,
    compressed_cache: bool = True,
):
    load_config()
    container_file_path = container_file_path or "images/custom/Containerfile"
    frappe_path = frappe_path or "https://github.com/frappe/frappe"
    frappe_branch = frappe_branch or "version-14"
    python_version = python_version or "3.10.5"
    node_version = node_version or "16.18.0"
    apps_json = apps_json or []
    kaniko_image = kaniko_image or "gcr.io/kaniko-project/executor:latest"
    apps_json_base64 = (
        base64.b64encode(
            json.dumps(apps_json).encode(),
        )
        .decode("utf-8")
        .replace("\n", "")
    )
    git_repo_context = git_repo_context or "github.com/frappe/frappe_docker"
    if not snapshot_mode:
        snapshot_mode = "redo"
    batch_v1_api = client.BatchV1Api()
    body = client.V1Job(api_version="batch/v1", kind="Job")
    body.metadata = client.V1ObjectMeta(
        namespace=namespace,
        name=job_name,
    )
    body.status = client.V1JobStatus()

    args = [
        f"--dockerfile={container_file_path}",
        f"--context=git://{git_repo_context}",
        f"--build-arg=FRAPPE_PATH={frappe_path}",
        f"--build-arg=FRAPPE_BRANCH={frappe_branch}",
        f"--build-arg=PYTHON_VERSION={python_version}",
        f"--build-arg=NODE_VERSION={node_version}",
        f"--build-arg=APPS_JSON_BASE64={apps_json_base64}",
        f"--destination={destination_image_name}",
        f"--snapshot-mode={snapshot_mode}",
    ]

    if use_new_run:
        args.append("--use-new-run")

    if insecure_registry:
        args.append("--insecure")

    if cache:
        args.append("--cache")

    if compressed_cache is not None:
        cc_flag = "true" if compressed_cache else "false"
        args.append(f"--compressed-caching={cc_flag}")

    body.spec = client.V1JobSpec(
        backoff_limit=0,
        template=client.V1PodTemplateSpec(
            spec=client.V1PodSpec(
                node_selector=node_selector,
                security_context=client.V1PodSecurityContext(
                    supplemental_groups=[1000]
                ),
                host_aliases=host_aliases,
                containers=[
                    client.V1Container(
                        name="build-worker",
                        image=kaniko_image,
                        args=args,
                        volume_mounts=[
                            client.V1VolumeMount(
                                mount_path="/kaniko/.docker",
                                name="container-config",
                            )
                        ],
                        resources=resources,
                    ),
                ],
                restart_policy="Never",
                volumes=[
                    client.V1Volume(
                        name="container-config",
                        projected=client.V1ProjectedVolumeSource(
                            sources=[
                                client.V1VolumeProjection(
                                    secret=client.V1SecretProjection(
                                        name=push_secret_name,
                                        items=[
                                            client.V1KeyToPath(
                                                key=".dockerconfigjson",
                                                path="config.json",
                                            ),
                                        ],
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
            ),
        ),
    )

    api_response = batch_v1_api.create_namespaced_job(
        namespace,
        body,
        pretty=True,
    )
    return to_dict(api_response)


def delete_job(name: str, namespace: str):
    load_config()
    batch_v1_api = client.BatchV1Api()
    api_response = batch_v1_api.delete_namespaced_job(
        name=name,
        namespace=namespace,
    )
    return to_dict(api_response)
