import os
import secrets
import time

import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic_settings import BaseSettings


# Use environment variables API_KEY and API_SECRET
class Settings(BaseSettings):
    api_key: str = "admin"
    api_secret: str = "changeit"
    enable_auth: bool = True


security = HTTPBasic(auto_error=False)

settings = Settings()


def validate_basic_auth(
    credentials: HTTPBasicCredentials = Depends(security),
):
    correct_api_key = None
    correct_api_secret = None
    if credentials and credentials.username:
        correct_api_key = secrets.compare_digest(
            credentials.username,
            settings.api_key,
        )
    if credentials and credentials.password:
        correct_api_secret = secrets.compare_digest(
            credentials.password,
            settings.api_secret,
        )
    if settings.enable_auth and not (correct_api_key and correct_api_secret):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect api_key or api_secret",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials


def validate_jwt(token: str):
    return jwt.decode(
        token,
        os.environ.get("API_SECRET", "changeit"),
        algorithms=["HS256"],
    )


def generate_token():
    payload = {
        "sub": os.environ.get("API_KEY", "admin"),
        "expires": round(time.time()) + 600,
    }
    return jwt.encode(
        payload,
        os.environ.get("API_SECRET", "changeit"),
        algorithm="HS256",
    )
