import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasicCredentials
from kubernetes.client.exceptions import ApiException

from k8s_bench.api.auth import validate_basic_auth
from k8s_bench.api.dtos import AddBenchDto, BenchDto, HelmSourceDto
from k8s_bench.client.flux import (
    add_flux_helm_release,
    add_flux_helm_source,
    delete_flux_helm_release,
    delete_flux_helm_source,
    get_helmrelease_status,
    update_flux_helm_source,
    update_flux_helm_release,
    get_helmsource_status,
)

router = APIRouter(prefix="/flux")


@router.get("/get-helmrelease-status")
def get_k8s_flux_helmrelease_status(
    name: str,
    namespace: str,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return get_helmrelease_status(
            name=name,
            namespace=namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/add-helmrelease")
def add_flux_bench(
    add_bench: AddBenchDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return add_flux_helm_release(
            name=add_bench.name,
            namespace=add_bench.namespace,
            release_interval=add_bench.release_interval,
            chart_interval=add_bench.chart_interval,
            chart=add_bench.chart,
            version=add_bench.version,
            source_ref=add_bench.source_ref.dict(),
            values=add_bench.values,
            remediate_last_failure=add_bench.remediate_last_failure,
            timeout=add_bench.timeout,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/update-helmrelease")
def update_flux_bench(
    add_bench: AddBenchDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return update_flux_helm_release(
            name=add_bench.name,
            namespace=add_bench.namespace,
            release_interval=add_bench.release_interval,
            chart_interval=add_bench.chart_interval,
            chart=add_bench.chart,
            version=add_bench.version,
            source_ref=add_bench.source_ref.dict(),
            values=add_bench.values,
            remediate_last_failure=add_bench.remediate_last_failure,
            timeout=add_bench.timeout,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-helmrelease")
def delete_flux_bench(
    bench: BenchDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_flux_helm_release(
            name=bench.name,
            namespace=bench.namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.get("/get-helmsource-status")
def get_k8s_flux_helmsource_status(
    name: str,
    namespace: str,
    kind: str,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return get_helmsource_status(
            name=name,
            namespace=namespace,
            kind=kind,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/add-helmsource")
def add_k8s_flux_helm_source(
    source: HelmSourceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return add_flux_helm_source(
            name=source.name,
            namespace=source.namespace,
            kind=source.kind,
            interval=source.interval,
            url=source.url,
            secret_ref=source.secret_ref,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/update-helmsource")
def update_k8s_flux_helm_source(
    source: HelmSourceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return update_flux_helm_source(
            name=source.name,
            namespace=source.namespace,
            kind=source.kind,
            interval=source.interval,
            url=source.url,
            secret_ref=source.secret_ref,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-helmsource")
def delete_k8s_flux_helm_source(
    source: HelmSourceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_flux_helm_source(
            name=source.name,
            namespace=source.namespace,
            kind=source.kind,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)
