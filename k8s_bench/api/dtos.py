import typing

from pydantic import BaseModel, Field


class AdditionalVolumesDto(BaseModel):
    volume_name: str
    pvc_name: str
    mount_path: str
    read_only: typing.Optional[bool] = None


class BenchCommandDto(BaseModel):
    job_name: str
    sites_pvc: str
    args: typing.List[str]
    command: typing.Optional[typing.List[str]] = None
    logs_pvc: typing.Optional[str] = None
    namespace: typing.Optional[str] = None
    image_pull_secrets: typing.Optional[typing.List[dict[str, str]]] = None
    image: typing.Optional[str] = None
    annotations: typing.Optional[dict] = None
    node_selector: typing.Optional[dict] = None
    resources: typing.Optional[dict] = None
    additional_volumes: typing.Optional[typing.List[AdditionalVolumesDto]] = (
        None  # noqa: E501
    )


class FrappeAppDto(BaseModel):
    url: str
    branch: str


class HostAliasDto(BaseModel):
    ip: str
    hostnames: typing.List[str]


class BuildBenchDto(BaseModel):
    job_name: str
    namespace: str
    push_secret_name: str
    destination_image_name: str
    container_file_path: typing.Optional[str] = None
    frappe_path: typing.Optional[str] = None
    frappe_branch: typing.Optional[str] = None
    python_version: typing.Optional[str] = None
    node_version: typing.Optional[str] = None
    apps_json: typing.Optional[typing.List[FrappeAppDto]] = None
    git_repo_context: typing.Optional[str] = None
    node_selector: typing.Optional[dict] = None
    insecure_registry: typing.Optional[bool] = None
    host_aliases: typing.Optional[typing.List[HostAliasDto]] = None
    snapshot_mode: typing.Optional[str] = None
    use_new_run: typing.Optional[bool] = None
    cache: typing.Optional[bool] = None
    resources: typing.Optional[dict] = None
    kaniko_image: typing.Optional[str] = None
    compressed_cache: typing.Optional[bool] = None


class FetchJobStatusDto(BaseModel):
    job_name: str
    namespace: str


class SourceRefDto(BaseModel):
    kind: str
    name: str
    namespace: str


class AddBenchDto(BaseModel):
    name: str
    namespace: str
    release_interval: str
    chart_interval: str
    chart: str = Field(
        description="""path to chart in case of git repo, e.g. "./erpnext"
         or chart name in case of helm repo, e.g. "erpnext"."""  #
    )
    source_ref: SourceRefDto
    values: dict
    remediate_last_failure: bool
    version: typing.Optional[str] = None
    timeout: typing.Optional[str] = None


class BenchDto(BaseModel):
    name: str
    namespace: str


class NamespaceDto(BaseModel):
    name: str


class SecretDto(BaseModel):
    name: str
    namespace: str
    string_data: typing.Optional[dict[str, str]] = None
    secret_type: typing.Optional[str] = None


class HelmSourceDto(BaseModel):
    name: str
    namespace: str
    kind: str
    interval: typing.Optional[str] = None
    url: typing.Optional[str] = None
    secret_ref: typing.Optional[str] = None


class IngressDto(BaseModel):
    name: str
    namespace: str
    host: typing.Optional[str] = None
    service_name: typing.Optional[str] = None
    service_port: typing.Optional[int] = None
    cert_secret_name: typing.Optional[str] = None
    is_wildcard: typing.Optional[bool] = None
    annotations: typing.Optional[dict[str, str]] = None


class EnvVarDto(BaseModel):
    name: str
    value: typing.Optional[str] = None
    value_from: typing.Optional[dict] = None


class CronJobDto(BaseModel):
    name: str
    namespace: str
    sites_pvc: str
    cronstring: str
    image: typing.Optional[str] = None
    args: typing.Optional[typing.List[str]] = None
    backoff_limit: int = 0
    command: typing.Optional[typing.List[str]] = None
    logs_pvc: typing.Optional[typing.Optional[str]] = None
    image_pull_secrets: typing.Optional[typing.List[dict[str, str]]] = None
    annotations: typing.Optional[dict] = None
    node_selector: typing.Optional[dict] = None
    resources: typing.Optional[dict] = None
    env_vars: typing.Optional[typing.List[EnvVarDto]] = None


class MetaDataDto(BaseModel):
    name: str
    namespace: str


class PodDto(BaseModel):
    name: str
    namespace: str
    image: str
    sites_pvc: str
    annotations: typing.Optional[dict] = None
    labels: typing.Optional[dict] = None
    command: typing.Optional[list[str]] = None
    args: typing.Optional[list[str]] = None
    image_pull_policy: str = "Always"
    image_pull_secrets: typing.Optional[typing.List[dict[str, str]]] = None
    ctr_name: str = "console"
    resources: typing.Optional[dict] = None
    logs_pvc: str | None = None
