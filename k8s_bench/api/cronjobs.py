import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasicCredentials
from kubernetes.client.exceptions import ApiException

from k8s_bench.api.auth import validate_basic_auth
from k8s_bench.api.dtos import (
    CronJobDto,
    MetaDataDto,
)
from k8s_bench.client.cronjobs import (
    add_cronjob,
    delete_cronjob,
    get_cronjob_status,
)

router = APIRouter(prefix="/cronjobs")


@router.post("/add")
def create_cronjob(
    cronjob: CronJobDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        data = cronjob.parse_obj(cronjob)
        env_vars = cronjob.dict().get("env_vars")
        return add_cronjob(
            name=data.name,
            namespace=data.namespace,
            sites_pvc=data.sites_pvc,
            cronstring=data.cronstring,
            args=data.args,
            image=data.image,
            backoff_limit=data.backoff_limit,
            command=data.command,
            logs_pvc=data.logs_pvc,
            image_pull_secrets=data.image_pull_secrets,
            annotations=data.annotations,
            node_selector=data.node_selector,
            resources=data.resources,
            env_vars=env_vars,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.get("/get-status")
def fetch_cronjob_status(
    name: str,
    namespace: str,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return get_cronjob_status(
            name=name,
            namespace=namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete")
def remove_cronjob(
    payload: MetaDataDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_cronjob(
            name=payload.name,
            namespace=payload.namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)
