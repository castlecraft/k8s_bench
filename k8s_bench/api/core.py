import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import HTTPBasicCredentials
from kubernetes.client.exceptions import ApiException

from k8s_bench.api.auth import validate_basic_auth
from k8s_bench.api.dtos import MetaDataDto, NamespaceDto, PodDto, SecretDto
from k8s_bench.client.core import (
    add_namespace,
    add_secret,
    create_pod,
    delete_namespace,
    delete_pod,
    delete_secret,
    update_secret,
    get_pod_status,
)

router = APIRouter(prefix="/core")


@router.post("/add-namespace")
def add_k8s_namespace(
    ns: NamespaceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return add_namespace(name=ns.name)
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-namespace")
def delete_k8s_namespace(
    ns: NamespaceDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_namespace(name=ns.name)
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/add-secret")
def add_k8s_secret(
    secret: SecretDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return add_secret(
            name=secret.name,
            namespace=secret.namespace,
            string_data=secret.string_data,
            secret_type=secret.secret_type,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-secret")
def delete_k8s_secret(
    secret: SecretDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_secret(name=secret.name, namespace=secret.namespace)
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/update-secret")
def update_k8s_secret(
    secret: SecretDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return update_secret(
            name=secret.name,
            namespace=secret.namespace,
            string_data=secret.string_data,
            secret_type=secret.secret_type,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/create-pod")
def create_k8s_pod(
    payload: PodDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return create_pod(
            name=payload.name,
            image_pull_secrets=payload.image_pull_secrets,
            namespace=payload.namespace,
            image=payload.image,
            sites_pvc=payload.sites_pvc,
            annotations=payload.annotations,
            labels=payload.labels,
            command=payload.command,
            args=payload.args,
            image_pull_policy=payload.image_pull_policy,
            ctr_name=payload.ctr_name,
            resources=payload.resources,
            logs_pvc=payload.logs_pvc,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.post("/delete-pod")
def delete_k8s_pod(
    payload: MetaDataDto,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return delete_pod(
            name=payload.name,
            namespace=payload.namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)


@router.get("/get-pod-status")
def fetch_job_status(
    name: str,
    namespace: str,
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    try:
        return get_pod_status(
            name=name,
            namespace=namespace,
        )
    except ApiException as e:
        logging.error(e)
        raise HTTPException(status_code=e.status, detail=e.body)
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500)
