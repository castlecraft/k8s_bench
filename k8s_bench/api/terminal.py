import asyncio
import logging
from typing import Union

from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    WebSocket,
    WebSocketDisconnect,
)
from fastapi.security import HTTPBasicCredentials

from k8s_bench.api.auth import (
    generate_token,
    validate_basic_auth,
    validate_jwt,
)
from k8s_bench.client.core import get_terminal

router = APIRouter(prefix="/terminal")


@router.get("/get-short-token")
def fetch_cronjob_status(
    credentials: HTTPBasicCredentials = Depends(validate_basic_auth),
):
    return {"token": generate_token()}


@router.websocket("/ws/{namespace}/{name}")
async def websocket_terminal(
    websocket: WebSocket,
    namespace: str,
    name: str,
    token: Union[str, None] = None,
    command: Union[str, None] = None,
    sleep: Union[int, None] = None,
    timeout: Union[int, None] = None,
):
    await websocket.accept()
    terminal = get_terminal(name, namespace, command=[command or "/bin/bash"])

    try:
        validate_jwt(token or "")
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=401, detail=str(e))

    try:
        while True:
            received_directive = await websocket.receive_text()
            evaluated_result = None

            while terminal.is_open():
                terminal.update(timeout=timeout or 10)
                terminal.write_stdin(received_directive + "\n")
                await asyncio.sleep(sleep or 1)

                if terminal.peek_stdout():
                    evaluated_result = terminal.read_stdout()

                if terminal.peek_stderr():
                    evaluated_result = terminal.read_stderr()

                if evaluated_result:
                    await websocket.send_text(evaluated_result)
                    evaluated_result = None
                    break
    except WebSocketDisconnect as e:
        logging.info(e)
