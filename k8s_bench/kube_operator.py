import json
import os
import logging

import kopf
from kubernetes.client.exceptions import ApiException

from k8s_bench.client.jobs import annotate_job
from k8s_bench.client.ingress import (
    create_ingress,
    delete_ingress,
    get_ingress,
    patch_ingress,
)

# Annotations
BASE_ANNOTATION = os.environ.get("BASE_ANNOTATION", "k8s-bench.castlecraft.in")
JOB_TYPE_ANNOTATION = f"{BASE_ANNOTATION}/job-type"
# job-type can be one of "create-site", "delete-site", "update-site"
INGRESS_NAME_ANNOTATION = f"{BASE_ANNOTATION}/ingress-name"
INGRESS_NAMESPACE_ANNOTATION = f"{BASE_ANNOTATION}/ingress-namespace"
INGRESS_HOST_ANNOTATION = f"{BASE_ANNOTATION}/ingress-host"
INGRESS_SVC_NAME_ANNOTATION = f"{BASE_ANNOTATION}/ingress-svc-name"
INGRESS_SVC_PORT_ANNOTATION = f"{BASE_ANNOTATION}/ingress-svc-port"
INGRESS_CERT_SECRET_ANNOTATION = f"{BASE_ANNOTATION}/ingress-cert-secret"
# Optional annotations
INGRESS_ANNOTATIONS_ANNOTATION = f"{BASE_ANNOTATION}/ingress-annotations"
INGRESS_IS_WILDCARD_ANNOTATION = f"{BASE_ANNOTATION}/ingress-is-widcard"
# System annotations
JOB_ANNOTATION_PROCESSED = f"{BASE_ANNOTATION}/is-job-processed"


@kopf.on.login()
def login_fn(**kwargs):
    return load_config(**kwargs)


@kopf.on.event("jobs")
def job_events_handler(event, **kwargs):
    job_type = get_annotations(event).get(JOB_TYPE_ANNOTATION)
    if job_type:
        is_job_complete = (
            event.get("object", {}).get("status", {}).get("succeeded")
        )  # noqa: E501
        is_processed = get_annotations(event).get(JOB_ANNOTATION_PROCESSED)
        if is_job_complete and not is_processed:
            if job_type == "create-site":
                create_site_ingress(event)
            elif job_type == "delete-site":
                delete_site_ingress(event)
            elif job_type == "update-site":
                update_site_ingress(event)


def load_config(**kwargs):
    if os.environ.get("KUBECONFIG"):
        return kopf.login_with_kubeconfig(**kwargs)
    else:
        return kopf.login_via_client(**kwargs)


def get_annotations(event):
    return event.get("object", {}).get("metadata", {}).get("annotations", {})


def create_site_ingress(event):
    ingress_name = get_annotations(event).get(INGRESS_NAME_ANNOTATION)  # noqa: E501
    ingress_namespace = get_annotations(event).get(
        INGRESS_NAMESPACE_ANNOTATION
    )  # noqa: E501
    ingress_host = get_annotations(event).get(INGRESS_HOST_ANNOTATION)  # noqa: E501
    ingress_svc_name = get_annotations(event).get(
        INGRESS_SVC_NAME_ANNOTATION
    )  # noqa: E501
    ingress_svc_port = get_annotations(event).get(
        INGRESS_SVC_PORT_ANNOTATION
    )  # noqa: E501
    ingress_cert_secret = get_annotations(event).get(
        INGRESS_CERT_SECRET_ANNOTATION
    )  # noqa: E501
    ingress_annotations = get_annotations(event).get(
        INGRESS_ANNOTATIONS_ANNOTATION
    )  # noqa: E501
    ingress_is_wildcard = get_annotations(event).get(
        INGRESS_IS_WILDCARD_ANNOTATION
    )  # noqa: E501

    if (
        ingress_name
        and ingress_namespace
        and ingress_host
        and ingress_svc_name
        and ingress_svc_port
        and ingress_cert_secret
    ):
        try:
            get_ingress(ingress_name=ingress_name, namespace=ingress_namespace)
        except ApiException as get_ingress_exc:
            if get_ingress_exc.status == 404:
                try:
                    response = create_ingress(
                        ingress_name=ingress_name,
                        namespace=ingress_namespace,
                        host=ingress_host,
                        service_name=ingress_svc_name,
                        service_port=int(ingress_svc_port),
                        cert_secret_name=ingress_cert_secret,
                        is_wildcard=ingress_is_wildcard,
                        annotations=(
                            json.loads(ingress_annotations)
                            if ingress_annotations
                            else None
                        ),
                    )
                    logging.info(json.dumps(response))
                    logging.info(
                        f"Created ingress/{ingress_name} in {ingress_namespace} namespace"  # noqa: E501
                    )
                    annotate_job_is_processed(event)
                except ApiException as create_ingress_exc:
                    logging.warn(json.dumps(create_ingress_exc))
                    logging.warn(
                        f"Failed to create ingress/{ingress_name} in {ingress_namespace} namespace"  # noqa: E501
                    )


def delete_site_ingress(event):
    ingress_name = get_annotations(event).get(INGRESS_NAME_ANNOTATION)
    ingress_namespace = get_annotations(event).get(
        INGRESS_NAMESPACE_ANNOTATION
    )  # noqa: E501
    if ingress_name and ingress_namespace:
        try:
            get_ingress(ingress_name=ingress_name, namespace=ingress_namespace)
            try:
                response = delete_ingress(
                    ingress_name=ingress_name,
                    namespace=ingress_namespace,
                )
                logging.info(json.dumps(response))
                logging.info(
                    f"Deleted ingress/{ingress_name} from {ingress_namespace} namespace"  # noqa: E501
                )
                annotate_job_is_processed(event)
            except ApiException as delete_ingress_exc:
                logging.warn(json.dumps(delete_ingress_exc))
                logging.warn(
                    f"Failed to delete ingress/{ingress_name} from {ingress_namespace} namespace"  # noqa: E501
                )
        except ApiException as get_ingress_exc:
            logging.warn(json.dumps(get_ingress_exc))
            logging.warn(
                f"Failed to get ingress/{ingress_name} from {ingress_namespace} namespace"  # noqa: E501
            )


def update_site_ingress(event):
    ingress_name = get_annotations(event).get(INGRESS_NAME_ANNOTATION)
    ingress_namespace = get_annotations(event).get(
        INGRESS_NAMESPACE_ANNOTATION
    )  # noqa: E501
    ingress_svc_name = get_annotations(event).get(INGRESS_SVC_NAME_ANNOTATION)
    ingress_svc_port = get_annotations(event).get(INGRESS_SVC_PORT_ANNOTATION)

    if (
        ingress_name
        and ingress_namespace
        and ingress_svc_name
        and ingress_svc_port  # noqa: E501
    ):
        try:
            get_ingress(ingress_name=ingress_name, namespace=ingress_namespace)
            try:
                response = patch_ingress(
                    ingress_name=ingress_name,
                    namespace=ingress_namespace,
                    service_name=ingress_svc_name,
                    service_port=int(ingress_svc_port),
                )
                logging.info(json.dumps(response))
                logging.info(
                    f"Patched ingress/{ingress_name} from {ingress_namespace} namespace"  # noqa: E501
                )
                annotate_job_is_processed(event)
            except ApiException as delete_ingress_exc:
                logging.warn(json.dumps(delete_ingress_exc))
                logging.warn(
                    f"Failed to delete ingress/{ingress_name} from {ingress_namespace} namespace"  # noqa: E501
                )
        except ApiException as get_ingress_exc:
            logging.warn(json.dumps(get_ingress_exc))
            logging.warn(
                f"Failed to get ingress/{ingress_name} from {ingress_namespace} namespace"  # noqa: E501
            )


def annotate_job_is_processed(event):
    job_name = event.get("object", {}).get("metadata", {}).get("name")
    job_namespace = (
        event.get("object", {}).get("metadata", {}).get("namespace")
    )  # noqa: E501
    if job_name and job_namespace:
        annotations = {}
        annotations[JOB_ANNOTATION_PROCESSED] = "true"
        try:
            response = annotate_job(
                job_name=job_name,
                namespace=job_namespace,
                annotations=annotations,  # noqa: E501
            )
            logging.info(json.dumps(response))
            logging.info(
                f"Anotate job/{job_name} from {job_namespace} namespace: {json.dumps(annotations)}"  # noqa: E501
            )
        except ApiException as exc:
            logging.warn(json.dumps(exc))
            logging.warn(
                f"Failed to anotate job/{job_name} from {job_namespace} namespace"  # noqa: E501
            )
