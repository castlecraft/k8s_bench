# Introduction

Operator and API to manage Frappe framework sites on Kubernetes.

**Operator** is used to watch Jobs with specific annotations. If the required annotations are found on Jobs, operator creates, patches or deletes ingress for the Frappe framework site.

**API** is optionally installed to have a service available to make ReST API calls to operate cluster related to benches.
