## Execute Bench Command

Example `curl` command:

```shell
curl -X POST http://0.0.0.0:8000/jobs/bench-command \
    -H 'Content-Type: application/json' \
    -u 'admin:changeit' \
    -d '{
  "job_name": "create-frappe-local",
  "sites_pvc": "frappe-bench-erpnext",
  "args": [
    "bench",
    "new-site",
    "--admin-password=admin",
    "--db-root-password=changeit",
    "--force",
    "--no-mariadb-socket",
    "frappe.localhost"
  ],
  "command": null,
  "logs_pvc": null,
  "namespace": "erpnext",
  "worker_image": "frappe/erpnext:v14.11.1",
  "annotations": {
    "k8s-bench.castlecraft.in/job-type": "create-site",
    "k8s-bench.castlecraft.in/ingress-name": "frappe-localhost",
    "k8s-bench.castlecraft.in/ingress-namespace": "erpnext",
    "k8s-bench.castlecraft.in/ingress-host": "frappe.localhost",
    "k8s-bench.castlecraft.in/ingress-svc-name": "frappe-bench-erpnext",
    "k8s-bench.castlecraft.in/ingress-svc-port": "8080",
    "k8s-bench.castlecraft.in/ingress-annotations": "{\"kubernetes.io/ingress.class\":\"nginx\"}",
    "k8s-bench.castlecraft.in/ingress-cert-secret": "frappe-certificate-tls"
  },
  "populate_assets": false
}'
```

- `job_name`: name of the `Job` to be created.
- `namespace`: namespace for running the `Job`
- `sites_pvc`: pvc name in the same namespace where job/helm release/bench is setup that holds all sites.
- `args`: array of args passed to container as command `CMD`.
- `command`: (Optional) array of commands passed to container as entrypoint `ENTRYPOINT`.
- `logs_pvc`: (Optional) pvc name in the same namespace where job/helm release/bench is setup that holds all logs.
- `worker_image`: name of worker image or image to use for pod/container executing the bench command.
- `nginx_image`: (Optional) name of nginx image if `populate_assets` is set to `true`. Used to copy assets to bench command container as static assets.
- `annotations`: list of additional annotations sent to job.
- `populate_assets`: if set to `true`, creates init container to copy assets from nginx container to bench command container.
- `node_selector`: key value pair of node selector, used to schedule job on labelled nodes.

Refer [API documentation](openapi.md) for details.
