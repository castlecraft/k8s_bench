# Maintainer guide


## Requirements

- Multi CPU and High memory Linux machine.
- [VS Code devcontainers](https://code.visualstudio.com/docs/devcontainers/containers)
- Podman, docker or anything that is supported by VS Code devcontainers
- gitlab runner for local execution of ci jobs

## Release

Prepare environment

```shell
python3 -m venv env
. ./env/bin/activate
pip install -e .
```

Execute release command:

```shell
./release.py --app {major|minor|patch} --chart {major|minor|patch}
```

Above command does following:

- bumps the app `--app` and chart `--chart` to the specified semver bump.
- if `--app` is bumped, `k8s_bench/__init__.py`, `docs/openapi.json` and `helm-chart/values.yaml` are updated.
- if `--chart` is bumped, `helm-chart/Chart.yaml` and `helm-chart/README.md` is updated.
- if both `--app` and `--chart` is bumped, `appVersion` will also be updated in `helm-chart/Chart.yaml`.

## Documentation

- Make sure the documentation and related assets are placed in `docs` directory
- Make sure `mkdocs.yml` from root of repo is edited for documentation index and metadata.
- Make sure Helm Chart README.md is updated.
- Gitlab pages will be generated if main branch updated and helm-charts or docs are affected.
- Pages job will generate mkdocs pages and generate helm chart.

## Code Quality Check

Make sure all the required code quality checks are included in `.pre-commit-config.yaml`. Use pre-commit for all checks.

## E2E Tests

local execution of `gitlab-runner` can verify if tests will pass online. Refer development docs for step by step testing. Make sure all the tests are added to CI yaml.

## Dependency updates

dependabot is configured, find configuration located at `.gitlab/dependabot.yml`
