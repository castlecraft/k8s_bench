# Contribution Guidelines

Before publishing a merge request, please test builds locally. E.g.,

```shell
gitlab-runner exec docker --docker-privileged test_e2e_merge_requests
```

On each merge request that contains changes relevant to builds, images are being built and tested in our CI (Gitlab CI).

> :evergreen_tree: Please be considerate when pushing commits and opening merge request for multiple branches, as the process of building images uses energy and contributes to global warming.

## Lint

We use `pre-commit` framework to lint the codebase before committing.
First, you need to install pre-commit with pip:

```shell
pip install pre-commit
```

Also you can use brew if you're on Mac:

```shell
brew install pre-commit
```

To setup _pre-commit_ hook, run:

```shell
pre-commit install
```

To run all the files in repository, run:

```shell
pre-commit run --all-files
```

Available targets can be found in `docker-bake.hcl`.

## Test

We use shell scripts for our integration tests.

Check the `test_e2e_merge_requests` pipeline job in `.gitlab-ci.yml` for sequence. Check the `tests` directory for test resources and scripts.

# Documentation

Place relevant markdown files in the `docs` directory and list it in `mkdocs.yml` located at the root of repo.
